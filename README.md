<div align="center">
<h1>mp4parser4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.58.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-88.5%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## <img alt="" src="./doc/assets/readme-icon-introduction.png" style="display: inline-block;" width=3%/> 介绍

一个读取、写入操作音视频文件编辑的工具。

### 特性

- 🚀 视频合成,裁剪,批量合成

- 🚀 音频合成,裁剪,批量合成

### 路线

<p align="center">
<img src="./doc/assets/milestone.png" width="100%" >
</p>

### 源码目录

```shell
.
├── README.md
├── doc
│   ├── assets
│   ├── cjcov
│   ├── feature_api.md
│   └── design.md
├── lib
├── src
│   ├── fileutils.cj  
│   └── mp4parsers.cj    
│       
└── test
    ├── HLT
    ├── LLT
    └── UT
```

- `doc` 存放库的设计文档、使用文档、LLT 用例覆盖报告
- `src` 是库源码目录
- `test` 存放 HLT 测试用例、LLT 自测用例和 UT 单元测试用例
- `lib` 是存放源码动态库

### 接口说明

主要是核心类和成员函数说明,详情见 [API](./doc/feature_api.md)

## <img alt="" src="./doc/assets/readme-icon-compile.png" style="display: inline-block;" width=3%/> 使用说明

### 编译

#### ffmpeg编译

进入ffmpeg根目录，请先在Linux或mysys2中执行以下命令

```shell
sed -i '/PROGS)/,/endif/d' Makefile && sed -i 's/%$(PROGSSUF)_g$(EXE/lib%$(PROGSSUF)$(SLIB/' Makefile && sed -i '/$(OBJS-$\*) $(FF_EXTRALIBS)/a \\t$(STRIP) $@' Makefile
sed -i 's/=%$(PROGSSUF).*$(EXESUF)/=lib%$(PROGSSUF)$(SLIBSUF)/g' fftools/Makefile && sed -i 's/$(1)$(PROGSSUF)_g$(EXESUF)/lib$(1)$(PROGSSUF)$(SLIBSUF)/g' fftools/Makefile && sed -i 's/$(LDFLAGS-$(1))/$(LDFLAGS-$(1)) -shared/' fftools/Makefile
set -i 's/int main/int ffmpeg_exec/' fftools/ffmpeg.c
sed -i 's/^int main/int ffmpeg_exec/' fftools/ffmpeg.c && sed -i '$a \\nint c_ffmpeg_exec(char \*str)\n\{\n    char \*tmp = strtok(str, " ");\n    int i = 0;\n    char \*argv[] = \{0\};\n    argv[i] = tmp;\n    while (tmp != NULL) {\n        i++;\n        tmp = strtok(NULL, " ");\n        argv[i] = tmp;\n    }\n    return ffmpeg_exec(i, argv);\n\}' fftools/ffmpeg.c
sed -i '/#endif.*FFTOOLS_FFMPEG_H/i\int c_ffmpeg_exec(char \*str);\nint ffmpeg_exec(int argc, char \*\*argv);\n' fftools/ffmpeg.h
```

1. linux x86_64 编译

   进入ffmpeg根目录，执行下面语句

   ```shell
   ./configure --disable-doc --disable-manpages --disable-htmlpages --disable-podpages --disable-txtpages --disable-ffplay --disable-ffprobe --extra-cflags=-fPIC --disable-asm
   make
   ```

2. Windows编译

   进入ffmpeg根目录，执行下面语句

   ```shell
   ./configure  --disable-doc --disable-manpages --disable-htmlpages --disable-podpages --disable-txtpages --disable-ffplay --disable-ffprobe --extra-cflags=-fPIC --disable-asm --arch=x86_64 --target-os=mingw32
   make
   ```


3. ohos编译

   进入ffmpeg根目录，执行下面语句（其中`/usr1/ohos/llvm/bin/`是ohos编译器路径，`/usr1/ohos/llvm/include/aarch64-linux-ohos`是ohos的头文件，请自行设置）

   ```shell
   ./configure --target-os=linux --arch=aarch64 --enable-cross-compile --disable-doc --disable-manpages --disable-htmlpages --disable-podpages --disable-txtpages --disable-ffplay --disable-ffprobe --extra-cflags=-fPIC --disable-asm --extra-cflags="-I/usr1/ohos/llvm/include/aarch64-linux-ohos --target=aarch64-linux-ohos" --extra-ldflags="--target=aarch64-linux-ohos" --cc="/usr1/ohos/llvm/bin/clang" --ar="/usr1/ohos/llvm/bin/llvm-ar" --ranlib="/usr1/ohos/llvm/bin/llvm-ranlib" --strip="/usr1/ohos/llvm/bin/llvm-strip"
   make
   ```

#### mp4parser编译

Linux编译

   将上面生成文件 `libcrypto.so`，放入根目录的 `lib` 文件夹下，之后执行

   ```
    git clone https://gitcode.com/Cangjie-TPC/mp4parser4cj.git  ---> 拉取代码
    cd mp4parser     ---> 进入到mp4parser4cj目录下
    cjpm build        ---> 编译
   ```

Windows编译

   将上面生成文件 `libcrypto.dll`，放入根目录的 `lib` 文件夹下，之后执行

   ```
   git clone https://gitcode.com/Cangjie-TPC/mp4parser4cj.git  ---> 拉取代码
   cd mp4parser     ---> 进入到mp4parser4cj目录下
   cjpm build        ---> 编译
   ```

### 功能示例

```cangjie
import std.io.*
import mp4parser.*
import std.os.posix.*
import std.fs.*

main() {
    var path2: String = getcwd()
    var path1: String = getcwd()
    var b = MP4Parser()
    var str = b.audioMultMerge("${path1}/a2.txt","${path1}/aa.mp3")
    sleep(Duration.second * 5)
    let fs: File = File("${path2}/aa.mp3",Open(true, false))
    var readFile: Array<UInt8> = Array<UInt8>()
    if (fs.canRead()) {
    	readFile = fs.readToEnd()
    	fs.flush()
    	fs.close()
    }
    if (readFile.size != 1048576) {
        return 1
    }
    println("success")
    return 0
}
```

运行结果如下：

```cangjie
success
```

## 开源协议

本项目基于 [Apache License 2.0](./LICENSE) ，请自由的享受和参与开源。

## <img alt="" src="./doc/assets/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。
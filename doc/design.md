### 三方库设计说明

#### 1 需求场景分析

一个读取、写入操作音视频文件编辑的工具。

#### 2 三方库对外提供的特性

（1） 视频合成,裁剪,批量合成。

（2） 音频合成,裁剪,批量合成。

#### 3 License分析

Apache License

|  Permissions   | Limitations  |
|  ----  | ----  |
| Commercial use | Liability |
| Modification  | Warranty |
| Distribution  |   |
| Private use   |   |

#### 4 依赖分析

```
标准库 
from std import ffi.c.*
from std import os.posix.*
from std import collection.*
from std import io.*
from std import log.*

```

#### 5 特性设计文档

##### 5.1 核心特性

    视频合成,裁剪,批量合成
    音频合成,裁剪,批量合成

###### 5.2 实现方案

    通过c_ffmpeg_exec的接口调用

###### 5.3 接口设计

💡 FileUtils 文件的路径判断。

| 成员函数 | 入参                     | 返回值 | 作用描述       |
| --- |------------------------| --- |------------|
| isFilePathValid  | filePath: String | (String,String) | 返回文件名和文件类型 |

💡 MP4Parser 操作音视频文件的工具。

| 成员函数 | 入参         | 返回值 | 作用描述               |
| --- |------------|  --- |--------------------|
| init   | 无          | Unit | 构建一个 MP4Parser 的实例 |
| videoClip   | startTime: String, endTime: String, sourcePath: String, outPath: String | Unit | mp4视频文件的剪切         |
| videoMultMerge   | sourcePath: String, outPath: String | Unit | mp4视频文件的批量合并       |
| videoMerge   | sourcePath: String, outPath: String | Unit | mp4视频文件的复制         |
| audioClip   | startTime: String, endTime: String, sourcePath: String, outPath: String | Unit | mp3音频文件的剪切         |
| audioMultMerge   | sourcePath: String, outPath: String | Unit | mp3音频文件的批量合并       |
| audioMerge   | sourcePath: String, outPath: String | Unit | mp3音频文件的复制         |

#### 🚀 6 架构图

<img alt="" src="./assets/example.png" style="display: inline-block;" width=60%/>








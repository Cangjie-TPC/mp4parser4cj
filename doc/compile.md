#### 编译
1.克隆ffmpeg,使用5.2版本

```
https://github.com/FFmpeg/FFmpeg.git
```
2.修改部分代码
```
cd ffmpeg
cd fftools
vim ffmpeg.c
//将4516行   int main(int argc, char **argv)  ——》int ffmpeg_exec(int argc, char **argv)
//4580行添加，保存退出。
int c_ffmpeg_exec(char *str)
{
    //printf("str = [%s]\n", str);
    char *tmp = strtok(str, " ");
    int i = 0;
    char *argv[] = {0};
    argv[i] = tmp;
    //printf("argv[%d] = [%s]\n", i, argv[i]);
    while (tmp != NULL)
    {
        i++;
        tmp = strtok(NULL, " ");
        argv[i] = tmp;
        //printf("argv[%d] = [%s]\n", i, argv[i]);
    }
    return ffmpeg_exec(i, argv);
}
vim ffmpeg.h
//703行添加，保存退出
int c_ffmpeg_exec(char *str);
int ffmpeg_exec(int argc, char **argv);
```
3. so 文件编译
```
cd ffmpeg/
chmod +x configure
./configure --enable-shared --disable-yasm --prefix=/home/cwl/work
chmod +x ./ffbuild/*.sh
make install
make clean
make
cd fftools/
gcc *.o -fpic -shared -o libffmpeg.so
cd /home/cwl/work/lib
cp -r ../ffmpeg/fftools/libffmpeg.so .
```
4. dll 文件编译
```
./configure --disable-doc --arch=x86_64 --target-os=mingw32
make -j20
//出现错误，收到执行下面命令
gcc -shared -Llibavcodec -Llibavdevice -Llibavfilter -Llibavformat -Llibavutil -Llibpostproc -Llibswscale -Llibswresample -Wl,--nxcompat,--dynamicbase -Wl,--high-entropy-va -Wl,--as-needed -Wl,--warn-common -Wl,-rpath-link=:libpostproc:libswresample:libswscale:libavfilter:libavdevice:libavformat:libavcodec:libavutil  -Wl,--image-base,0x140000000 -o libffmpeg.dll fftools/ffmpeg_filter.o fftools/ffmpeg_hw.o fftools/ffmpeg_mux.o fftools/ffmpeg_opt.o  fftools/cmdutils.o fftools/opt_common.o fftools/ffmpeg.o  -lavdevice -lavfilter -lavformat -lavcodec -lswresample -lswscale -lavutil  -lpsapi -lole32 -lstrmiids -luuid -loleaut32 -lshlwapi -lgdi32 -lm -latomic -lvfw32 -lm -latomic -lm -latomic -lz -lsecur32 -lws2_32 -lm -latomic -lmfuuid -lole32 -lstrmiids -lole32 -luser32 -lz -lm -latomic -lm -latomic -lm -luser32 -lbcrypt -latomic  -lole32 -lpsapi -lshell32
```


## MP4Parser库

#### 介绍

一个读取、写入操作音视频文件编辑的工具

#### 1. 提供mp4文件格式的视频合成,裁剪功能

##### 1.1 视频文件的剪切

```cangjie
/**
 * 无参 MP4Parser 构造器
 */
public init()
/**
 * 视频文件的剪切
 * 
 * @param startTime 视频的起始时间(00:00:24)
 * @param endTime 剪切音频的时间长度(00:01:24)
 * @param sourcePath 原文件路径(/home/a.mp4)
 * @param outPath 输出文件路径(/home/b.mp4)
 * @param return 成功返回值1，失败返回值0
 */
public func videoClip(startTime: String, endTime: String, sourcePath: String, outPath: String): Int32
/**
 * 无参 FileUtils 构造器
 */
public init()
/**
 * 判断文件路径
 * 
 * @param filePath 传入的是文件路径
 * @return 第一个String是文件名，第二个String是文件类型
 */
public func isFilePathValid(filePath: String): (String,String)
```

##### 1.2 视频文件的批量合并

```cangjie
/**
 * 视频文件的批量合并
 * 
 * @param sourcePath 原文件路径(conxt.txt)
 * @param outPath 输出文件路径(/home/a.mp4)
 * @param return 成功返回值1，失败返回值0
 */
public func videoMultMerge(sourcePath: String, outPath: String): Int32
```

##### 1.3 视频文件的合并

```cangjie
/**
 * 视频文件的合并
 * 
 * @param sourcePath 原文件路径(/home/b.mp4)
 * @param outPath 输出文件路径(/home/a.mp4)
 * @param return 成功返回值1，失败返回值0
 */
public func videoMerge(sourcePath: String, outPath: String): Int32
```

##### 1. 示例

```cangjie
from std import io.*
from mp4parser import mp4parser.*
from std import os.posix.*
from std import fs.*

main() {
    var path2: String = getcwd()
    var b = MP4Parser()
    sleep(1000*1000*11)
    var str = b.videoMultMerge("${path2}/a1.txt","./aa.mp4")
    let fs: File = File("${path2}/a.mp4",Open(true, false))
    var readFile: Array<UInt8> = Array<UInt8>()
    if (fs.canRead()) {
    	readFile = fs.readToEnd()
    	fs.flush()
    	fs.close()
    }
    if (readFile.size != 11440055) {
        return 1
    }
    println("success")
    return 0
}
```
执行结果如下：

```shell
success
```

#### 2. 提供音频合成,裁剪功能

##### 2.1 音频文件的剪切

```cangjie
/**
 * 音频文件的剪切
 * 
 * @param startTime 音频的起始时间(00:00:24)
 * @param endTime 剪切音频的时间长度(00:01:24)
 * @param sourcePath 原文件路径(/home/a.mp3)
 * @param outPath 输出文件路径(/home/b.mp3)
 * @param return 成功返回值1，失败返回值0
 */
public func audioClip(startTime: String, endTime: String, sourcePath: String, outPath: String): Int32
```

##### 2.2 音频文件的批量合并

```cangjie
/**
 * 音频文件的批量合并
 * 
 * @param sourcePath 原文件路径(conxt.txt)
 * @param outPath 输出文件路径(/home/a.mp3)
 * @param return 成功返回值1，失败返回值0
 */
public func audioMultMerge(sourcePath: String, outPath: String): Int32
```

##### 2.3 音频文件的合并

```cangjie
/**
 * 音频文件的合并
 * 
 * @param sourcePath 原文件路径(/home/a.mp3)
 * @param outPath 输出文件路径(/home/b.mp3)
 * @param return 成功返回值1，失败返回值0
 */
public func audioMerge(sourcePath: String, outPath: String): Int32
```

##### 2. 示例

```cangjie
from std import io.*
from mp4parser import mp4parser.*
from std import os.posix.*
from std import fs.*

main() {
    var path2: String = getcwd()
    var path1: String = getcwd()
    var b = MP4Parser()
    sleep(1000*1000*11)
    var str = b.audioMultMerge("${path1}/a2.txt","${path1}/aa.mp3")
    let fs: File = File("${path2}/aa.mp3",Open(true, false))
    var readFile: Array<UInt8> = Array<UInt8>()
    if (fs.canRead()) {
        readFile = fs.readToEnd()
        fs.flush()
        fs.close()
    }
    if (readFile.size != 1048576) {
        return 1
    }
    println("success")
    return 0
}
```
执行结果如下：

```shell
success
```
